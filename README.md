# A Developmental Study of Weather

This is developed as an [ipython](http://ipython.org/) document.

However, you may not need ipython to view it, as there is a [rendered HTML document](https://github.com/shakna-israel/DevelopmentalStudyOfWeather/blob/master/A%20Developmental%20Study%20of%20Weather.html), however it is not interactive.

If you wish to contribute, you will need ipython.

Two builds that give you ipython and a lot more scientific tools stand out:

* [Anaconda](http://continuum.io/downloads)
* [Canopy](https://store.enthought.com/downloads/)

However, ipython can be installed via pip:

```
pip install ipython
```

And then the notebook can be opened in a browser:

```
ipython notebook
```